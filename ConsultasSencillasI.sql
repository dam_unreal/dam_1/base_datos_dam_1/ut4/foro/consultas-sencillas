USE empleadosdepart;

#1
SELECT apellido, oficio, dept_no FROM emple;
#2
SELECT dept_no, dnombre, loc FROM depart;
#3
SELECT * FROM emple;
#4
SELECT * FROM emple ORDER BY apellido;
#5
SELECT * FROM emple ORDER BY dept_no DESC;
#6
SELECT * FROM emple ORDER BY dept_no DESC,  apellido;
#7
SELECT * FROM emple WHERE salario>2000;
#8
SELECT * FROM emple WHERE oficio='ANALISTA';
#9
SELECT apellido, oficio FROM emple WHERE dept_no=20;
#10
SELECT * FROM emple ORDER BY fecha_alt;
#11
SELECT * FROM emple WHERE oficio='VENDEDOR' ORDER BY apellido;
#12
SELECT * FROM emple WHERE dept_no=10 AND oficio='ANALISTA' ORDER BY apellido;
#13
SELECT * FROM emple WHERE salario>2000 OR dept_no=20;
#14
SELECT * FROM emple ORDER BY oficio, apellido;
#15
SELECT * FROM emple WHERE apellido LIKE 'A%';
#16
SELECT * FROM emple WHERE apellido LIKE '%Z';
#17
SELECT * FROM emple WHERE apellido LIKE 'A%' AND oficio LIKE '%E%';
#18
SELECT * FROM emple WHERE salario BETWEEN 1000 AND 2000;
#19
SELECT * FROM emple WHERE oficio='VENDEDOR' AND comision>100;
#20
SELECT * FROM emple ORDER BY dept_no, apellido;
#21
SELECT emp_no, apellido FROM emple WHERE apellido LIKE '%Z' AND salario>3000;
#22
SELECT * FROM depart WHERE loc LIKE 'C%';
#23
SELECT * FROM emple WHERE oficio='EMPLEADO' AND salario>1000 AND dept_no='10';
#24
SELECT apellido FROM emple WHERE comision IS NULL;
#25
SELECT apellido FROM emple WHERE comision IS NULL AND apellido LIKE 'J%';
#26
SELECT apellido FROM emple WHERE oficio IN ('VENDEDOR','ANALISTA','EMPLEADO');
#27
SELECT apellido FROM emple WHERE NOT oficio IN ('ANALISTA','EMPLEADO') AND salario>2000;
#28
SELECT * FROM emple WHERE salario BETWEEN 2000 AND 3000;
#29
SELECT apellido, salario, dept_no FROM emple WHERE salario>2000 AND dept_no IN ('10','30');
#30
SELECT apellido, emp_no FROM emple WHERE NOT salario BETWEEN 1000 AND 2000;
#31
SELECT LOWER(apellido) FROM emple;
#32
SELECT CONCAT(apellido, oficio) FROM emple;
#33
SELECT apellido, CHAR_LENGTH(apellido) FROM emple ORDER BY CHAR_LENGTH(apellido) DESC;
#34
SELECT apellido, GREATEST(salario, comision) FROM emple;
#35
SELECT * FROM emple WHERE dept_no IN (10) AND comision IS NULL;
