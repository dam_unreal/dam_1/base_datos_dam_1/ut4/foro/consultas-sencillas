DROP SCHEMA IF EXISTS empleados_hospital;
CREATE SCHEMA IF NOT EXISTS empleados_hospital DEFAULT CHARACTER SET utf8;
USE empleados_hospital;

create table empleados_hospital(
  cod_hospital	int,
  dni 		int primary key,
  nombre	varchar(50),
  funcion	varchar(30),
  salario	int,
  localidad	varchar(20));

insert into empleados_hospital values(1,12345678,'Garcia Hernandez, Eladio','CONSERJE',1200,'DOS HERMANAS');
insert into empleados_hospital values(1,87654321,'Fuentes Bermejo, Carlos','DIRECTOR',2000,'SEVILLA');
insert into empleados_hospital values(2,55544433,'Gonzalez Marin, Alicia','CONSERJE',1200,'SEVILLA');
insert into empleados_hospital values(1,66655544,'Castillo Montes, Pedro','MEDICO',1700,'ALMERIA');
insert into empleados_hospital values(2,22233322,'Tristan Garcia, Ana','MEDICO',1900,'SEVILLA');
insert into empleados_hospital values(3,55544411,'Ruiz Hernandez, Caridad','MEDICO',1900,'SEVILLA');
insert into empleados_hospital values(3,99988333,'Serrano Diaz, Alejandro','DIRECTOR',2400,'DOS HERMANAS');
insert into empleados_hospital values(4,33222111,'Mesa del Castillo, Juan','MEDICO',2200,'LOS PALACIOS');
insert into empleados_hospital values(2,22233333,'Martinez Molina, Andres','MEDICO',1600,'LOS PALACIOS');
insert into empleados_hospital values(4,55544412,'Jimenez Jimenez, Dolores','CONSERJE',1200,'DOS HERMANAS');
insert into empleados_hospital values(4,22233311,'Martinez Molina, Gloria','MEDICO',1600,'SEVILLA');

COMMIT;
